#!/bin/bash
set -euo pipefail
set -x

rm -rfv \
    /tmp/gitaly.git \
    /tmp/gitaly-unpacked.git

git clone --bare --single-branch --no-tags git@gitlab.com:gitlab-org/gitaly.git /tmp/gitaly.git
master_sha=$(git -C /tmp/gitaly.git rev-parse HEAD)

git init --bare /tmp/gitaly-unpacked.git
git -C /tmp/gitaly-unpacked.git unpack-objects </tmp/gitaly.git/objects/pack/*.pack
# bundle etc. will ignore it if I have no ref ptr
git -C /tmp/gitaly-unpacked.git update-ref refs/heads/master $master_sha
git -C /tmp/gitaly-unpacked.git rev-list master >/tmp/gitaly-unpacked.git/rev-list.txt
# The server sent us some extra crap in its pack, get rid of it for subsequent simplicity
git -C /tmp/gitaly-unpacked.git prune

(
    cd /tmp/gitaly-unpacked.git

    du -shc objects/?? >size.loose

    # Create from loose & delta on the fly
    git bundle create master-from-loose.bundle master

    split -l 1000 rev-list.txt rev-list.txt-
    for chunk in rev-list.txt-*
    do
        git pack-objects --thin "objects/pack/pack-$chunk" <$chunk
    done

    # show that we'll only read from these packs now
    rm -rf objects/??

    du -shc objects/pack/*.pack >size.split-packs

    git bundle create master-from-split-packs.bundle master
)




