#!/bin/bash
set -euo pipefail
#set -x

rm -rf /tmp/gitlab-test.git
git  init --template="" --bare /tmp/gitlab-test.git
cd /tmp/gitlab-test.git
mkdir hooks
cat <<-\EOF >hooks/reference-transaction
#!/bin/bash
set -euo pipefail

echo "$$ -> args: <$@>" >>tx-$WHAT
while read line
do
	echo "$$ -> Got line <$line>" >>tx-$WHAT
done
EOF
chmod +x hooks/reference-transaction
id="fetch-as-push-hack-$RANDOM"

WHAT=FETCH git fetch --no-tags https://gitlab.com/gitlab-org/gitlab-test "refs/*:refs/hidden/$id/*"
WHAT=FETCH-PUSH git push --atomic "file://$PWD" "refs/hidden/$id/*:refs/*"

echo "Fetch TXs:"
awk '{print $1}' /tmp/gitlab-test.git/tx-FETCH | sort -u | wc -l
echo "Fetch->Push TXs:"
awk '{print $1}' /tmp/gitlab-test.git/tx-FETCH-PUSH | sort -u | wc -l
