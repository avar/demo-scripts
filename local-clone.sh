#!/bin/bash
# Run local-commits.sh and local-clone.sh together
set -euo pipefail
set -x

while true
do
    rm -rf /tmp/local-commits-clone
    git clone --no-hardlinks --local /tmp/local-commits /tmp/local-commits-clone
    git -C /tmp/local-commits-clone for-each-ref
done
