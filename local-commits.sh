#!/bin/bash
# Run local-commits.sh and local-clone.sh together
set -euo pipefail
set -x

rm -rf /tmp/local-commits
git init --template="" /tmp/local-commits
cd /tmp/local-commits
while true
do
    echo . >>f
    git add f
    git commit -m"bump"
done
